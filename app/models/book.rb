class Book < ApplicationRecord

	has_many :borrows

	before_save { self.title = title.downcase }

	validates :title, :author, presence: true
	validates :title, uniqueness: { case_sensitive: false }
	validates_inclusion_of :state_machine, :in => %w( available unavailable )
	validates :quantity, numericality: {greater_than_or_equal_to: 0}
	validates :quantity, numericality: {less_than_or_equal_to: :quantity_total}
  	validates :quantity_total, numericality: { greater_than_or_equal_to: :quantity}
  	validate :check_quantity

  	def check_quantity
  		if self.quantity_total != self.quantity + self.borrows.length
  			errors.add(:quantity, "Book quantity invalid.")
  		end
  	end

  	def self.stats
  		_stats = ActiveRecord::Base.connection.execute("select sum(quantity), sum(quantity_total) from books;").values.first
	  	stats = {available: _stats[0], unavailable: _stats[1] - _stats[0], total: _stats[1]}
	end

	def switch_state_machine
		if self.quantity == 0 && self.available?
			self.state_machine = "unavailable"
		elsif self.quantity != 0 && !self.available?
			self.state_machine = "available"
		end
	end

	def increase_quantity
		self.quantity = self.quantity + 1
	end

	def decrease_quantity
		self.quantity = self.quantity - 1
	end

	def can_be_returned?
		self.quantity < self.quantity_total
	end

	def available?
		self.state_machine == "available"
	end
end

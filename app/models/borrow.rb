class Borrow < ApplicationRecord

	delegate :email, to: :user, prefix: true, allow_nil: true
	delegate :title, to: :book, prefix: true, allow_nil: true
	belongs_to :user
	belongs_to :book

	validates_uniqueness_of :user_id, :scope => :book_id

end

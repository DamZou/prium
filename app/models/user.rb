class User < ApplicationRecord
	
	has_many :borrows

	mount_uploader :photo, PhotoUploader

	before_save { self.email = email.downcase }

	validates :first_name, :last_name, :email, presence: true
	validates :email, uniqueness: { case_sensitive: false }
	validates_inclusion_of :state_machine, :in => %w( not_borrower borrower )

	def self.stats
		_stats = User.all.group(:state_machine).count
  		borrowers = _stats['borrower'] || 0
  		not_borrowers = _stats['not_borrower'] || 0
	  	stats = {borrower: borrowers, not_borrower: not_borrowers, total: borrowers + not_borrowers}
	end

	def switch_state_machine
		if self.borrows.length == 0 && self.state_machine == 'borrower'
			self.state_machine = 'not_borrower'
		elsif self.borrows.length != 0 && self.state_machine == 'not_borrower'
			self.state_machine = "borrower"
		end
	end

	def borrower?
		self.state_machine == "borrower"
	end

	def get_books
      books = Array.new
      if self.borrows.length > 0
        self.borrows.each do |borrow|
          books.push({title: borrow.book.title})
        end
      else
        books.push({title: I18n.t('borrow.new.json.books')})
      end
      books
    end

end

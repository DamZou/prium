$(document).on('turbolinks:load', function() {
	$("#user_email").autocomplete({
		source: $("#user_email").data('autocomplete-source')
	});
	
	$("#book_title").autocomplete({
		source: $("#book_title").data('autocomplete-source')
	});
	
	$("#user_email").focusout(function(){
	    $.ajax({
		  url: "/fr/email",
		  type: 'GET',
		  dataType: 'json',
		  data: {
		  	email: $("#user_email").val()
		  },
		  success: function(result) {
			console.log(result)
		  	if(jQuery.type(result.user) !== 'string') {
		  		$("#name").text(result.user.first_name + ' ' + result.user.last_name);
				$("#books").empty();
				$.each(result.books, function(key, value) {
					$("#books").append('<li>' + value.title + '</li>');
				});
		  	} else {
		  		$("#name").text(result.user);
		  		$("#books").empty();
		  	}
		  },
		  error: function(result) {
		  	//console.log('error', result)
		  },
		});
	});
});
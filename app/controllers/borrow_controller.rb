class BorrowController < ApplicationController
	before_action :init_error, except: [:new, :return_book], if: :error?
	before_action :set_user, only: [:destroy, :create]
	before_action :set_book, only: [:destroy, :create]

	def new
		@button_name = t('borrow_book.submit')
		@url = borrow_index_path
		@borrow = Borrow.new
	end

	def return_book
		@button_name = t('return_book.submit')
		@url = borrow_destroy_path
		@borrow = Borrow.new
		render 'new'
	end

	def create
		if @error.length == 0
			if @book.available?
				@borrow = Borrow.new(book: @book, user: @user)
				if @borrow.save
					@user.switch_state_machine
					@user.save
					@book.decrease_quantity
					@book.switch_state_machine
					@book.save
				else
					@error.push(t('controller.borrow.user_has_book'))
				end
			else
				@error.push(t('controller.borrow.book_unavailable'))
			end	
		end
	end

	def destroy
		if @error.length == 0
			if @book.can_be_returned?
				if set_borrow
					if @borrow.destroy
						@user.switch_state_machine
						@user.save
						@book.increase_quantity
						@book.switch_state_machine
						@book.save
					end
				end
			else
				@error.push(t('controller.borrow.book_available'))
			end
		end
	end

	private

	def error?
		if !@error
			true
		else
			false
		end
	end
	
	def set_user
		@user = User.find_by(:email => params[:borrow][:user_email])
		if @user == nil
			@error.push(t('controller.borrow.invalid_user'))
		end
	end

	def set_book
		@book = Book.find_by(:title => params[:borrow][:book_title])
		if @book == nil
			@error.push(t('controller.borrow.invalid_book'))
		end
	end

	def init_error
		if !@error
			@error = Array.new
		end
	end

	def set_borrow
		@borrow = Borrow.find_by(book: @book, user: @user)
		if @borrow
			true
		else
			@error.push(t('controller.borrow.user_has_not_book'))
			false
		end
	end

	def borrow_params
		params.require(:borrow).permit(:user_id, :book_id)
	end

end

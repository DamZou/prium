class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  def index
    @users = User.order(:email).where("email like ?", "%#{params[:term]}%")
    respond_to do |format|
      format.html
      format.json { render json: @users.map{ |user| {:label => user.email} }}
    end
  end

  def show
  end

  def new
    @user = User.new
  end

  def edit
  end

  def create
    @user = User.new(user_params)
    @user.save
  end

  def update
    @user.update(user_params)
  end

  def destroy
    if !@user.borrower?
      @user.destroy
      @check = true
    end
  end

  def by_email
     user = User.find_by_email(params[:email])
     if user
       books = user.get_books
     else 
       user = t('borrow.new.json.users')
     end
     result = {user: user, books: books}
     render json: result
  end

  private

    def set_user
      @user = User.find(params[:id])
    end

    def user_params
      params.require(:user).permit(:last_name, :first_name, :email, :photo, :state_machine)
    end
end

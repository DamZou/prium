class BooksController < ApplicationController
  before_action :set_book, only: [:show, :edit, :update, :destroy]

  def index
    @books = Book.order(:title).where("title like ?", "%#{params[:term]}%")
    respond_to do |format|
      format.html
      format.json { render json: @books.map{ |book| {:label => book.title} }}
    end
  end

  def show
  end

  def new
    @book = Book.new
  end

  def edit
  end

  def create
    @book = Book.new(book_params)
    @book.switch_state_machine
    @book.save
  end

  def update
    @book.update(book_params)
    temp_state_machine = @book.state_machine
    @book.switch_state_machine
    if @book.state_machine != temp_state_machine
      @book.save
    end
  end

  def destroy
    if !@book.can_be_returned?
      @book.destroy
      @check = true
    end
  end

  private

  def set_book
    @book = Book.find(params[:id])
  end

  def book_params
    params.require(:book).permit(:title, :author, :summary, :state_machine, :user, :quantity, :quantity_total)
  end
  
end

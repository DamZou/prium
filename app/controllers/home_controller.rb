class HomeController < ApplicationController
	
	def index
		book_stats = Book.stats
		@nb_books_available = book_stats[:available]
		@nb_books_unavailable = book_stats[:unavailable]
		@nb_books = book_stats[:total]
		user_stats = User.stats
		@nb_users_borrower = user_stats[:borrower]
		@nb_users_not_borrower = user_stats[:not_borrower]
		@nb_users = user_stats[:total]
	end

end
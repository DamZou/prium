require 'rails_helper'

RSpec.describe "Borrow's routes", type: :routing do
    ["fr", 'en'].each do |locale|
        it 'route post :locale/borrow ' + locale + '.' do
            expect(:post => '/' + locale + '/borrow').to route_to(:controller => "borrow",
                                                    :action => "create",
                                                    :locale => locale
                                                )
        end

        it 'route get :locale/borrow/new ' + locale + '.' do
            expect(:get => '/' + locale + '/borrow/new').to route_to(:controller => "borrow",
                                                    :action => "new",
                                                    :locale => locale
                                                )
        end

        it 'route get :locale/return/book ' + locale + '.' do
            expect(:get => '/' + locale + '/return/book').to route_to(:controller => "borrow",
                                                    :action => "return_book",
                                                    :locale => locale
                                                )
        end

        it 'route post :locale/return ' + locale + '.' do
            expect(:post => '/' + locale + '/return').to route_to(:controller => "borrow",
                                                    :action => "destroy",
                                                    :locale => locale
                                                )
        end
	end
end
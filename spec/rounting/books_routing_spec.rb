require 'rails_helper'

RSpec.describe "Books's routes", type: :routing do
    ["fr", 'en'].each do |locale|
        it 'route get :locale/books ' + locale + '.' do
            expect(:get => '/' + locale + '/books').to route_to(:controller => "books",
                                                    :action => "index",
                                                    :locale => locale
                                                )
        end

        it 'route post :locale/books ' + locale + '.' do
            expect(:post => '/' + locale + '/books').to route_to(:controller => "books",
                                                    :action => "create",
                                                    :locale => locale
                                                )
        end

        it 'route get :locale/books/new ' + locale + '.' do
            expect(:get => '/' + locale + '/books/new').to route_to(:controller => "books",
                                                    :action => "new",
                                                    :locale => locale
                                                )
        end

        it 'route get :locale/books/:id/edit ' + locale + '.' do
            expect(:get => '/' + locale + '/books/1/edit').to route_to(:controller => "books",
                                                    :action => "edit",
                                                    :locale => locale,
                                                    :id => '1'
                                                )
        end

        it 'route get :locale/books/:id ' + locale + '.' do
            expect(:get => '/' + locale + '/books/1').to route_to(:controller => "books",
                                                    :action => "show",
                                                    :locale => locale,
                                                    :id => '1'
                                                )
        end

        it 'route patch :locale/books/:id ' + locale + '.' do
            expect(:patch => '/' + locale + '/books/1').to route_to(:controller => "books",
                                                    :action => "update",
                                                    :locale => locale,
                                                    :id => '1'
                                                )
        end

        it 'route put :locale/books/:id ' + locale + '.' do
            expect(:put => '/' + locale + '/books/1').to route_to(:controller => "books",
                                                    :action => "update",
                                                    :locale => locale,
                                                    :id => '1'
                                                )
        end

        it 'route delete :locale/books/:id ' + locale + '.' do
            expect(:delete => '/' + locale + '/books/1').to route_to(:controller => "books",
                                                    :action => "destroy",
                                                    :locale => locale,
                                                    :id => '1'
                                                )
        end
	end
end
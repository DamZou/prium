require 'rails_helper'

RSpec.describe "Users's routes", type: :routing do
    ["fr", 'en'].each do |locale|
        it 'route get :locale/users ' + locale + '.' do
            expect(:get => '/' + locale + '/users').to route_to(:controller => "users",
                                                    :action => "index",
                                                    :locale => locale
                                                )
        end

        it 'route post :locale/users ' + locale + '.' do
            expect(:post => '/' + locale + '/users').to route_to(:controller => "users",
                                                    :action => "create",
                                                    :locale => locale
                                                )
        end

        it 'route get :locale/users/new ' + locale + '.' do
            expect(:get => '/' + locale + '/users/new').to route_to(:controller => "users",
                                                    :action => "new",
                                                    :locale => locale
                                                )
        end

        it 'route get :locale/users/:id/edit ' + locale + '.' do
            expect(:get => '/' + locale + '/users/1/edit').to route_to(:controller => "users",
                                                    :action => "edit",
                                                    :locale => locale,
                                                    :id => '1'
                                                )
        end

        it 'route get :locale/users/:id ' + locale + '.' do
            expect(:get => '/' + locale + '/users/1').to route_to(:controller => "users",
                                                    :action => "show",
                                                    :locale => locale,
                                                    :id => '1'
                                                )
        end

        it 'route patch :locale/users/:id ' + locale + '.' do
            expect(:patch => '/' + locale + '/users/1').to route_to(:controller => "users",
                                                    :action => "update",
                                                    :locale => locale,
                                                    :id => '1'
                                                )
        end

        it 'route put :locale/users/:id ' + locale + '.' do
            expect(:put => '/' + locale + '/users/1').to route_to(:controller => "users",
                                                    :action => "update",
                                                    :locale => locale,
                                                    :id => '1'
                                                )
        end

        it 'route delete :locale/users/:id ' + locale + '.' do
            expect(:delete => '/' + locale + '/users/1').to route_to(:controller => "users",
                                                    :action => "destroy",
                                                    :locale => locale,
                                                    :id => '1'
                                                )
        end

        it 'route get :locale/email ' + locale + '.' do
            expect(:get => '/' + locale + '/email').to route_to(:controller => "users",
                                                    :action => "by_email",
                                                    :locale => locale
                                                )
        end
	end
end
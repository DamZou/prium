require 'rails_helper'

RSpec.describe "Home's routes", type: :routing do
    ["fr", 'en'].each do |locale|
        it 'route get :locale ' + locale + '.' do
            expect(:get => '/' + locale).to route_to(:controller => "home",
                                                    :action => "index",
                                                    :locale => locale
                                                )
        end
	end
end
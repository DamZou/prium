require "rails_helper"

RSpec.describe Book, type: :model do
	it "Title name cannot be blank." do
		book = create_book
		book[:title] = nil
		expect(Book.new(book)).to_not be_valid
	end

	it "Author cannot be blank." do
		book = create_book
		book[:author] = nil
		expect(Book.new(book)).to_not be_valid
	end

	it "State can only be 'available' or 'not_available." do
		book = create_book
		book[:state_machine] = "state_machine"
		expect(Book.new(book)).to_not be_valid
	end

	it "Book invalid model." do
		book = create_book
		expect(Book.new(book)).to be_valid
	end

	it "Title must be unique." do
		book_same_title = create_book_same_title
		Book.create(create_book)
		expect(Book.new(book_same_title)).to_not be_valid
	end

	it "Quantity must be numericality." do
		book = create_book
		book[:quantity] = "zero"
		expect(Book.new(book)).to_not be_valid
	end

	it "Quantity_total must be numericality." do
		book = create_book
		book[:quantity_total] = "zero"
		expect(Book.new(book)).to_not be_valid
	end

	it "Quantity must be less than quantity_total." do
		book = create_book
		book[:quantity] = 1
		expect(Book.new(book)).to_not be_valid
	end

	it "Increase_quantity must increase quantity." do
		book = Book.create(create_book)
		book.increase_quantity
		expect(book.quantity).to eq(1)
	end

	it "Decrease_quantity must decrease quantity." do
		book = Book.create(create_book)
		book[:quantity] = 1
		book.decrease_quantity
		expect(book.quantity).to eq(0)
	end

	it "Available? must return true." do
		book = Book.create(create_book)
		expect(book.available?).to eq(true)
	end

	it "Available? must return false." do
		book = Book.create(create_book)
		book[:state_machine] = 'unavailable'
		expect(book.available?).to eq(false)
	end

	it "Can_be_returned? must return false." do
		book = Book.create(create_book)
		expect(book.can_be_returned?).to eq(false)
	end

	it "Can_be_returned? must return true." do
		book = Book.create(create_book)
		book[:quantity_total] = 1
		expect(book.can_be_returned?).to eq(true)
	end

	it "Switch_state_machine don't switch to unavailable." do
		book = Book.create(create_book)
		book[:quantity] = 0
		book.switch_state_machine
		expect(book.state_machine).to eq('unavailable')
	end

	it "Switch_state_machine don't switch to unavailable." do
		book = Book.create(create_book)
		book[:quantity] = 1
		book[:state_machine] = 'unavailable'
		book.switch_state_machine
		expect(book.state_machine).to eq('available')
	end

	it "self.stats is false" do
		create_book3
		expect(Book.stats).to eq({available: 4, unavailable: 0, total: 4})
	end

	it "validate quantity is false" do
		book = create_book2
		user = create_user
		borrow = Borrow.create(book: book, user: user)
		book.borrows.push(borrow)
		expect(book.validate).to eq(true)
	end

	def create_book
		{
		 :title => "title", 
		 :author => 'author',
		 :quantity => 0,
		 :quantity_total => 0,
		 :id => 1
		}
	end

	def create_book2
		Book.create({
		 :title => "title2", 
		 :author => 'author2',
		 :quantity => 2,
		 :quantity_total => 3,
		 :id => 2
		})
	end

	def create_book3
		Book.create({
		 :title => "title3", 
		 :author => 'author3',
		 :quantity => 4,
		 :quantity_total => 4
		})
	end

	def create_book_same_title
		{
		 :title => "title", 
		 :author => 'author_same_title',
		 :quantity => 0,
		 :quantity_total => 0
		}
	end

	def create_user
		User.create({
		 :first_name => "first_name", 
		 :last_name => 'last_name',
		 :email => 'email',
		 :id => 1
		})
	end
end
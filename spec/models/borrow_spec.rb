require "rails_helper"

RSpec.describe Borrow, type: :model do
	it "Book_id cannot be blank." do
		create_book
		create_user
		borrow = create_borrow
		borrow[:book_id] = nil
		expect(Borrow.new(borrow)).to_not be_valid
	end

	it "User_id cannot be blank." do
		create_book
		create_user
		borrow = create_borrow
		borrow[:user_id] = nil
		expect(Borrow.new(borrow)).to_not be_valid
	end

	it "Borrow invalid model." do
		create_book
		create_user
		borrow = create_borrow
		expect(Borrow.new(borrow)).to be_valid
	end

	it "User_id and book_id must be unique." do
		create_book
		create_user
		borrow_same = create_borrow
		Borrow.create(create_borrow)
		expect(Borrow.new(borrow_same)).to_not be_valid
	end

	def create_borrow
		{
		 :user_id => 1, 
		 :book_id => 1
		}
	end

	def create_book
		Book.create({
		 :id => 1,
		 :title => "title", 
		 :author => 'author',
		 :quantity => 0,
		 :quantity_total => 0
		})
	end

	def create_user
		User.create({
		 :id => 1,
		 :first_name => "first_name", 
		 :last_name => 'last_name',
		 :email => 'email'
		})
	end
end
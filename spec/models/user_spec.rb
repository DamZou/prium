require "rails_helper"

RSpec.describe User, type: :model do
	it "First name cannot be blank." do
		user = create_user
		user[:first_name] = nil
		expect(User.new(user)).to_not be_valid
	end

	it "Last_name cannot be blank." do
		user = create_user
		user[:last_name] = nil
		expect(User.new(user)).to_not be_valid
	end

	it "Email cannot be blank." do
		user = create_user
		user[:email] = nil
		expect(User.new(user)).to_not be_valid
	end

	it "State can only be 'borrower' or 'not_borrower." do
		user = create_user
		user[:state_machine] = "state_machine"
		expect(User.new(user)).to_not be_valid
	end

	it "User invalid model." do
		user = create_user
		expect(User.new(user)).to be_valid
	end

	it "Email must be unique." do
		user_same_email = create_user_same_email
		User.create(create_user)
		expect(User.new(user_same_email)).to_not be_valid
	end

	it "Switch_state_machine don't switch to borrower." do
		user = User.create(create_user)
		create_book
		create_borrow
		user.switch_state_machine
		expect(user.state_machine).to eq('borrower')
	end

	it "Switch_state_machine don't switch to not_borrower." do
		user = User.create(create_user)
		user[:state_machine] = 'borrower'
		user.switch_state_machine
		expect(user.state_machine).to eq('not_borrower')
	end

	it "Borrower? must return true." do
		user = User.create(create_user)
		user[:state_machine] = 'borrower'
		expect(user.borrower?).to eq(true)
	end

	it "Borrower? must return false." do
		user = User.create(create_user)
		expect(user.borrower?).to eq(false)
	end

	it "self.stats is false" do
		user = User.create(create_user)
		create_user2
		expect(User.stats).to eq({borrower: 1, not_borrower: 1, total: 2})
	end

	it "get_books must return 'Pas de livres" do
		user = User.create(create_user)
		expect(user.get_books).to eq([{:title=> I18n.t('borrow.new.json.books')}])

	end

	it "get_books must return 'Pas de livres" do
		user = User.create(create_user)
		create_book
		create_borrow
		expect(user.get_books).to eq([{:title=>"title"}])
	end

	def create_user
		{
		 :first_name => "first_name", 
		 :last_name => 'last_name',
		 :email => 'email',
		 :id => 1
		}
	end

	def create_user2
		User.create({
		 :first_name => "first_name2", 
		 :last_name => 'last_name2',
		 :email => 'email2',
		 :id => 2,
		 :state_machine => 'borrower'
		})
	end

	def create_book
		Book.create({
		 :title => "title", 
		 :author => 'author',
		 :quantity => 0,
		 :quantity_total => 0,
		 :id => 1
		})
	end

	def create_borrow
		Borrow.create({
		 :user_id => 1, 
		 :book_id => 1,
		 :id => 1
		})
	end

	def create_user_same_email
		{
		 :first_name => "first_name_same_email", 
		 :last_name => 'last_name_same_email',
		 :email => 'email'
		}
	end
end
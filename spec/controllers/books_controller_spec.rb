require "rails_helper"

RSpec.describe BooksController, :type => :controller do
	["fr", 'en'].each do |locale|
		describe "GET #index" do
			it "responds successfully with an HTTP 200 status code" do
				get :index, {:locale => locale}
				expect(response).to be_success
				expect(response).to have_http_status(200)
			end

			it "renders the index template" do
				get :index, {:locale => locale}
				expect(response).to render_template("index")
			end

			it "loads all of the books into @books" do
				book1 = Book.create(create_book)
				book2 = create_book2
				get :index, {:locale => locale}

				expect(assigns(:books)).to match_array([book1, book2])
			end

			def create_book
				{
					:title => "title", 
					:author => 'author',
					:quantity => 0,
					:quantity_total => 0
				}
			end

			def create_book2
				Book.create({
					:title => "title2", 
					:author => 'author2',
					:quantity => 1,
					:quantity_total => 1
					})
			end
		end
	end
end

require "rails_helper"

RSpec.describe UsersController, :type => :controller do
  ["fr", 'en'].each do |locale|
	  describe "GET #index" do
	    it "responds successfully with an HTTP 200 status code" do
	      get :index, {:locale => locale}
	      expect(response).to be_success
	      expect(response).to have_http_status(200)
	    end

	    it "renders the index template" do
	      get :index, {:locale => locale}
	      expect(response).to render_template("index")
	    end

	    it "loads all of the users into @users" do
	      user1 = User.create(create_user)
	      user2 = create_user2
	      get :index, {:locale => locale}

	      expect(assigns(:users)).to match_array([user1, user2])
	    end

	    def create_user
			{
			 :first_name => "first_name", 
			 :last_name => 'last_name',
			 :email => 'email',
			 :id => 1
			}
		end

		def create_user2
			User.create({
			 :first_name => "first_name2", 
			 :last_name => 'last_name2',
			 :email => 'email2',
			 :id => 2,
			 :state_machine => 'borrower'
			})
		end
	  end
	end
end
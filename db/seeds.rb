# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
15.times do |num|
	num += 1
	Book.create(title: "Book #{num}", 
				author: "Author book #{num}", 
				summary: "Summary book #{num}",
				quantity: num,
				quantity_total: num)
end

15.times do |num|
	num += 1
	User.create(last_name: "Last name #{num}", 
				first_name: "First name #{num}", 
				email: "Email #{num}")
end

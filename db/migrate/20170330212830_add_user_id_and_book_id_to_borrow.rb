class AddUserIdAndBookIdToBorrow < ActiveRecord::Migration[5.0]
  def change
    add_column :borrows, :user_id, :integer
    add_column :borrows, :book_id, :integer
  end
end

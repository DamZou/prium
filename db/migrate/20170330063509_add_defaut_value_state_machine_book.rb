class AddDefautValueStateMachineBook < ActiveRecord::Migration[5.0]
  def change
  	change_column_default :books, :state_machine, "available"
  end
end

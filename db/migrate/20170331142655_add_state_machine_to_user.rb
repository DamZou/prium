class AddStateMachineToUser < ActiveRecord::Migration[5.0]
  def change
    add_column :users, :state_machine, :string, :default => "not_borrower"
  end
end

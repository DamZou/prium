class AddQuantityAndQuantityTotalToBook < ActiveRecord::Migration[5.0]
  def change
    add_column :books, :quantity, :integer
    add_column :books, :quantity_total, :integer
  end
end

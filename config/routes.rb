Rails.application.routes.draw do
  
  scope "/:locale" do
	  root "home#index"
	  
	  resources :books
	  
	  resources :users
	  get 'email', to: 'users#by_email', as: :email

	  resources :borrow, only: [:new, :create]
	  get 'return/book' , to: 'borrow#return_book', as: :return
	  post 'return', to: 'borrow#destroy', as: :borrow_destroy
	end
end
